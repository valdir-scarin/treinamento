package br.com.skalena.treinamento.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.skalena.treinamento.app.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class Aplicacao {

	public static void main(String[] args) {
        SpringApplication.run(Aplicacao.class, args);
	}
}