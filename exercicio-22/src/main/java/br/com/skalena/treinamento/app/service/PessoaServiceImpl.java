package br.com.skalena.treinamento.app.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PessoaServiceImpl implements PessoaService {

	@Autowired
	private PessoaRepository repository;

	@Override
	public Optional<Pessoa> obterPessoa(Integer id) {
		return repository.findById(id);
	}

	@Override
	public Collection<Pessoa> obterTodasPessoas() {
		return repository.findAll();
	}

}