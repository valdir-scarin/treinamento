package br.com.skalena.treinamento.app.web;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.service.PessoaService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/pessoa")
@Slf4j
public class PessoaControllerImpl implements PessoaController {

	private static final String LAST_ID = "ultimo_id";

	@Autowired
	private PessoaService service;
	
	@Autowired
	private ObjectMapper mapper;
	
	@GetMapping("/{id}")
	@ResponseBody
	public Optional<Pessoa> obterPessoa(@PathVariable("id") Integer id) {
		return service.obterPessoa(id);
	}

	@GetMapping("/teste/{id}")
	@ResponseBody
	public String obterPessoaAnterior(@PathVariable("id") Integer id, HttpSession session) {
		String retorno;
		try {
			retorno = session.getAttribute(LAST_ID) + "\n" + mapper.writeValueAsString(service.obterPessoa(id));
		} catch (JsonProcessingException e) {
			retorno = "erro";
		}
		session.setAttribute(LAST_ID, id);
		return retorno;
	}
	
}