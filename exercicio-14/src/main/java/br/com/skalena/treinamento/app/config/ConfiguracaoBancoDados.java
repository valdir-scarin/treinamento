package br.com.skalena.treinamento.app.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "br.com.skalena.treinamento.app.repository"
					 , entityManagerFactoryRef = "entityManagerFactory"
					 , transactionManagerRef = "transactionManager")
public class ConfiguracaoBancoDados {

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName( "org.h2.Driver");
		ds.setUrl("jdbc:h2:mem:data;DB_CLOSE_DELAY=-1");
		ds.setUsername("sa");
		ds.setPassword("password");
		return ds;
	}

	@Bean
	public NamedParameterJdbcTemplate namedParamJdbcTemplate(DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}

	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean emfb = new LocalContainerEntityManagerFactoryBean();
		emfb.setDataSource(dataSource);
		emfb.setPackagesToScan("br.com.skalena.treinamento.app.entity");
		emfb.setPersistenceProviderClass(HibernatePersistenceProvider.class);
		Properties props = new Properties();
		props.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
		props.put("hibernate.format_sql", true);
		props.put("hibernate.show_sql", false);
		props.put("hibernate.hbm2ddl.auto", "create");
		emfb.setJpaProperties(props);
		return emfb;
	}

	@Bean(name = "transactionManager")
	public JpaTransactionManager transactionmanager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}
}