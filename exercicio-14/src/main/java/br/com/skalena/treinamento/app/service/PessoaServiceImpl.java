package br.com.skalena.treinamento.app.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.skalena.treinamento.app.entity.Pessoa;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PessoaServiceImpl implements PessoaService {

	@Value("${skalena.treinamento.pessoa.nome-padrao}")
	private String nomePadrao;

	@Override
	public Pessoa obterPessoa(Integer id) {
		log.info("obterPessoa - inicio");
		Pessoa pessoa = new Pessoa();
		pessoa.setId(id);
		pessoa.setNome(getNomePadrao());
		log.info("obterPessoa - fim");
		return pessoa;
	}

	@Override
	public Collection<Pessoa> obterTodasPessoas() {
		log.info("obterPessoa - inicio");
		List<Pessoa> pessoas = Arrays.asList(obterPessoa(1), obterPessoa(2));
		log.info("obterPessoa - fim");
		return pessoas;
	}

	public String getNomePadrao() {
		return nomePadrao;
	}

}