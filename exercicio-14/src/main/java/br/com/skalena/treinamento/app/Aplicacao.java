package br.com.skalena.treinamento.app;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import br.com.skalena.treinamento.app.config.Configuracao;
import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Aplicacao {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Configuracao.class);

		PessoaRepository repository = app.getBean(PessoaRepository.class);

		printPessoas(repository.findAll());

		for (int i = 1; i < 22; i++) {
			repository.save(new Pessoa(i, "exercicio " + i, "lista"));
		}
		
		printPessoas(repository.findByNomeAndSobrenome("exercicio 1", "lista"));

		printPessoas(repository.findIdImpar());

		printPessoas(repository.findIdImpar(PageRequest.of(0, 2)).getContent());
		
		printPessoas(repository.findIdImpar(PageRequest.of(0, 2, Sort.by(Order.desc("id")))).getContent());

		app.close();
	}

	private static void printPessoas(Pessoa pessoa) {
		log.info("pessoa  - " + pessoa);
	}

	private static void printPessoas(List<Pessoa> pessoas) {
		log.info("total linhas = " + pessoas.size());
		int i = 1;
		for (Pessoa pessoa : pessoas) {
			log.info("pessoa  " + i++ + " - " + pessoa);
		}
	}

}