package br.com.skalena.treinamento.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.skalena.treinamento.app.entity.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {

	Pessoa findByNome(String nome);
	
}
