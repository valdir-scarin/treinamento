package br.com.skalena.treinamento.app;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import br.com.skalena.treinamento.app.config.Configuracao;
import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Aplicacao {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Configuracao.class);
		
		PessoaRepository repository = app.getBean(PessoaRepository.class);
		
		log.info(repository.toString());
		
		log.info("total linhas=" + repository.findAll().size());
		
		repository.save(new Pessoa(1, "exercicio 3"));
		
		log.info("total linhas=" + repository.findAll().size());

		app.close();
	}

}