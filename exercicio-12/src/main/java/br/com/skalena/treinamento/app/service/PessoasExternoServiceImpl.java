package br.com.skalena.treinamento.app.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Primary
@Component
public class PessoasExternoServiceImpl extends PessoaServiceImpl {

	@Value("${skalena.treinamento.pessoa.nome-padrao-externo}")
	private String nomePadrao;
	
	@Override
	public String getNomePadrao() {
		return nomePadrao;
	}

}