# Problema apresentado

Criar uma aplica��o em spring que inicialize um bean que tem dois m�todos: obterPessoa(Integer id) e obterTodasPessoas.
O valor dos m�todos devem escritos hard-code, por�m o nome padr�o da pessoa deve ser preenchido com valor descrito em um arquivo de propriedades.
A classe pessoa deve ter dois atributos: nome e id.

Voc� deve escrever as seguintes classes:

* Aplicacao.class: 
	- ela ir� inicializar o contexto do Spring por meio da AnnotationConfigApplicationContext;
	- ela ir� requisitar um bean do tipo PessoaService.class para o Spring;
* Configuracao.java: ela ser� respons�vel por configurar os beans da aplica��o e carregar o arquivo de configura��o;
* PessoaService.java & PessoaServiceImpl.java: faz a cria��o dos objetos pessoas usando como referencia o id passado e o nome padr�o.
* Pessoa.java: bean com dois atributos id e nome.

Desafio:

1. Configurar dois beans do mesmo tipo (PessoaService) no contexto do spring e fazer com que ele escolha automaticamente o bean retornado.
2. Configurar o lombok e usar as anota��es @Getter, @Setter e @Slf4j
