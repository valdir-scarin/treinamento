package br.com.skalena.treinamento.app.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.service.PessoaService;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/pessoa")
@Slf4j
public class PessoaControllerImpl implements PessoaController {

	@Autowired
	private PessoaService service;

	@Override
	@GetMapping("/pesquisar")
	public String pesquisar(@RequestParam(name = "nome", required = false) String nome, ModelMap model) {
		model.addAttribute("pessoas", service.obterPessoa(nome));
		return "pessoa/pesquisar";
	}

	@Override
	@GetMapping("/detalhar/{id}")
	public String detalhar(@PathVariable(name = "id") Integer id, ModelMap model) {
		model.addAttribute("pessoa", service.obterPessoa(id));
		return "pessoa/dados";
	}

	@Override
	@GetMapping("/pre-incluir")
	public String preIncluir(ModelMap model) {
		model.addAttribute("bean", new Pessoa());
		return "pessoa/dados";
	}

	@Override
	@PostMapping("/incluir")
	public String incluir(@Valid Pessoa pessoa, ModelMap model) {
		model.addAttribute("msg", "Sucesso");
		return pesquisar(null, model);
	}

	@Override
	@GetMapping("/pre-alterar/{id}")
	public String preAlterar(@PathVariable(name = "id") Integer id, ModelMap model) {
		model.addAttribute("pessoa", service.obterPessoa(id));
		return "pessoa/dados";
	}

	@Override
	@GetMapping("/alterar/{id}")
	public String acao(@Valid Pessoa pessoa, ModelMap model) {
		model.addAttribute("msg", "Sucesso");
		return pesquisar(null, model);
	}

	@Override
	@GetMapping("/excluir/{id}")
	public String excluir(@PathVariable(name = "id") Integer id, ModelMap model) {
		service.obterPessoa(id)
			   .ifPresent(a-> service.excluir(a.getId()));
		return pesquisar(null, model);
	}

	@Override
	@GetMapping("/{id}")
	@ResponseBody
	public Pessoa obter(@PathVariable(name = "id") Integer id) {
		return service.obterPessoa(id).get();
	}

}