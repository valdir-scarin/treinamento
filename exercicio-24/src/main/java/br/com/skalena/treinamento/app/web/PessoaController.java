package br.com.skalena.treinamento.app.web;

import javax.validation.Valid;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.skalena.treinamento.app.entity.Pessoa;

public interface PessoaController {

	String pesquisar(String nome, ModelMap model);

	Pessoa obter(Integer id);

	String excluir(Integer id, ModelMap model);

	String preIncluir(ModelMap model);
	
	String incluir(Pessoa pessoa, ModelMap model);

	String alterar(@Valid Pessoa pessoa, ModelMap model);

	String preAlterar(ModelMap model);

	String detalhar(@PathVariable(name = "id") Integer id, ModelMap model);

	String preAlterar(Integer id, ModelMap model);

}
