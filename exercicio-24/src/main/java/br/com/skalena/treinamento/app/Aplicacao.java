package br.com.skalena.treinamento.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@EnableAutoConfiguration
@EnableWebMvc
@Slf4j
public class Aplicacao extends SpringBootServletInitializer {

	public static void main(String[] args) {
        SpringApplication.run(Aplicacao.class, args);
	}
}