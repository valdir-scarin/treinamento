package br.com.skalena.treinamento.app;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class Aplicacao implements CommandLineRunner {

	@Autowired
	private PessoaRepository repository;

	public static void main(String[] args) {
		log.info("STARTING THE APPLICATION");
        SpringApplication.run(Aplicacao.class, args);
        log.info("APPLICATION FINISHED");
	}

	private void printPessoas(Pessoa pessoa) {
		log.info("pessoa  - " + pessoa);
	}

	private void printPessoas(List<Pessoa> pessoas) {
		log.info("total linhas = " + pessoas.size());
		int i = 1;
		for (Pessoa pessoa : pessoas) {
			log.info("pessoa  " + i++ + " - " + pessoa);
		}
	}

	@Override
	public void run(String... args) throws Exception {
		printPessoas(repository.findAll());

		for (int i = 1; i < 22; i++) {
			repository.save(new Pessoa(i, "exercicio " + i, "lista", LocalDate.now()));
		}

		printPessoas(repository.findByNomeAndSobrenome("exercicio 1", "lista"));

		printPessoas(repository.findIdImpar());

		printPessoas(repository.findIdImpar(PageRequest.of(0, 2)).getContent());

		printPessoas(repository.findIdImpar(PageRequest.of(0, 2, Sort.by(Order.desc("id")))).getContent());
	}

}